<?php

namespace App\Action\Carrinho;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class Adicionar
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $produto = $request->getParsedBody()['produto'];

        if (isset($_SESSION['carrinho'][$produto])) {
            $_SESSION['carrinho'][$produto]++;
        } else {
            $_SESSION['carrinho'][$produto] = 1;
        }

        $response = $response->withRedirect('/carrinho', 301);

        return $response;
    }
}
