<?php

namespace App\Action\Carrinho;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\ORM\EntityManager as EM;

final class Listar
{
    private $view;
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, Twig $view, EM $em)
    {
        $this->view   = $view;
        $this->logger = $logger;
        $this->em     = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $produtosRepo = $this->em->getRepository('App\Entity\Produto');
        $chaves = array_keys($_SESSION['carrinho']);
        $itens = $produtosRepo->findBy(['id' => $chaves]);

        $this->view->render($response, 'carrinho/listar.html', ['itens' => $itens]);

        return $response;
    }
}
