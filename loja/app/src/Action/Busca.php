<?php

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\ORM\EntityManager as EM;
use Doctrine\Common\Collections\Criteria;

final class Busca
{
    private $view;
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, Twig $view, EM $em)
    {
        $this->view   = $view;
        $this->logger = $logger;
        $this->em     = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $produtosRepo = $this->em->getRepository('App\Entity\Produto');

        $termo = $request->getParsedBody()['busca'];

        $termo_sanitizado = filter_var($termo, FILTER_SANITIZE_STRING);

        $criteria = Criteria::create()
            ->where(Criteria::expr()->contains("nome", $termo))
            ->orWhere(Criteria::expr()->contains("descricao", $termo));

        $produtos = $produtosRepo->matching($criteria);

        $this->view->render($response, 'busca.html', ['produtos' => $produtos, 'termo' => $termo_sanitizado]);

        return $response;
    }
}
