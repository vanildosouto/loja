<?php

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\ORM\EntityManager as EM;

final class Categoria
{
    private $view;
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, Twig $view, EM $em)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $categoriaRepo = $this->em->getRepository('App\Entity\Categoria');

        $categoria = $categoriaRepo->findOneBy(['slug' => $args['categoria']]);

        $this->view->render($response, 'categoria.html', [
            'title' => ' - ' . $categoria->getNome(),
            'categoria' => $categoria,
        ]);

        return $response;
    }
}
