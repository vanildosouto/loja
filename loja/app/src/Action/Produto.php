<?php

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\ORM\EntityManager as EM;

final class Produto
{
    private $view;
    private $logger;
    private $em;

    public function __construct(LoggerInterface $logger, Twig $view, EM $em)
    {
        $this->view   = $view;
        $this->logger = $logger;
        $this->em     = $em;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $produto = $this->em->find('App\Entity\Produto', $args['id']);

        $template = 'produto.html';
        if (count($produto) == 0) {
            $template = 'semproduto.html';
        }

        $this->view->render($response, $template, ['produto' => $produto]);

        return $response;
    }
}
