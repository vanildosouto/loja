<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="categorias")
 */
class Categoria
{
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="Produto", mappedBy="categorias")
     */
    private $produtos;

    public function __construct()
    {
        $this->produtos = new ArrayCollection();
    }

    public function setId($id)
    {
        return $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNome($nome)
    {
        return $this->nome = $nome;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setSlug($slug)
    {
        return $this->slug = $slug;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setProdutos($produtos)
    {
        return $this->produtos = $produtos;
    }

    public function getProdutos()
    {
        return $this->produtos;
    }
}
