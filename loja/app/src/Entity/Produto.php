<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="produtos")
 */
class Produto
{
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $nome;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var text
     */
    private $descricao;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $imagem;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float
     */
    private $preco;

    /**
     * @ORM\ManyToMany(targetEntity="Categoria", inversedBy="produtos")
     * @ORM\JoinTable(name="produto_categoria")
     */
    private $categorias;

    /**
     * @ORM\ManyToMany(targetEntity="Pedido", mappedBy="produtos")
     */
    private $pedidos;

    public function __construct()
    {
        $this->categorias = new ArrayCollection();
        $this->pedidos = new ArrayCollection();
    }

    public function setId($id)
    {
        return $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNome($nome)
    {
        return $this->nome = $nome;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setDescricao($descricao)
    {
        return $this->descricao = $descricao;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setImagem($imagem)
    {
        return $this->imagem = $imagem;
    }

    public function getImagem()
    {
        return $this->imagem;
    }

    public function setPreco($preco)
    {
        return $this->preco = $preco;
    }

    public function getPreco()
    {
        return $this->preco;
    }

    public function setCategorias($categorias)
    {
        return $this->categorias = $categorias;
    }

    public function getCategorias()
    {
        return $this->categorias;
    }
}
