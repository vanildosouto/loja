<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="enderecos")
 */
class Endereco
{
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var string
     */
    private $endereco;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @var string
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     * @var string
     */
    private $complemento;

    /**
     * @ORM\Column(type="integer", length=8, nullable=false)
     * @var integer
     */
    private $cep;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var string
     */
    private $bairro;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var string
     */
    private $cidade;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     * @var string
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="endereco")
     */
    private $pedidos;

    public function setId($id)
    {
        return $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEndereco($endereco)
    {
        return $this->endereco = $endereco;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function setNumero($numero)
    {
        return $this->numero = $numero;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setComplemento($complemento)
    {
        return $this->complemento = $complemento;
    }

    public function getComplemento()
    {
        return $this->complemento;
    }

    public function setCep($cep)
    {
        $cep = preg_replace("/[^0-9]/", "", $cep);
        return $this->cep = $cep;
    }

    public function getCep()
    {
        return $this->cep;
    }

    public function setBairro($bairro)
    {
        return $this->bairro = $bairro;
    }

    public function getBairro()
    {
        return $this->bairro;
    }

    public function setCidade($cidade)
    {
        return $this->cidade = $cidade;
    }

    public function getCidade()
    {
        return $this->cidade;
    }

    public function setEstado($estado)
    {
        return $this->estado = $estado;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setPedidos($pedidos)
    {
        return $this->pedidos = $pedidos;
    }

    public function getPedidos()
    {
        return $this->pedidos;
    }

    public function __construct()
    {
        $this->pedidos = new ArrayCollection();
    }
}
