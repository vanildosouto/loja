<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuarios")
 */
class Usuario
{
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @var string
     */
    private $nome;

    /**
     * @ORM\Column(type="integer", length=11, nullable=false)
     * @var integer
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @var string
     */
    private $senha;

    /**
     * @ORM\ManyToMany(targetEntity="Endereco")
     * @ORM\JoinTable(name="usuario_enderecos",
     *      joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="endereco_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $enderecos;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="usuario")
     */
    private $pedidos;

    public function setId($id)
    {
        return $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNome($nome)
    {
        return $this->nome = $nome;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setCpf($cpf)
    {
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        return $this->cpf = $cpf;
    }

    public function getCpf()
    {
        return $this->cpf;
    }

    public function setEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Endereço de email inválido");
        }

        return $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setSenha($senha)
    {
        $senha = \password_hash($senha, PASSWORD_BCRYPT);
        return $this->senha = $senha;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setEnderecos($enderecos)
    {
        return $this->enderecos = $enderecos;
    }

    public function getEnderecos()
    {
        return $this->enderecos;
    }

    public function setPedidos($pedidos)
    {
        return $this->pedidos = $pedidos;
    }

    public function getPedidos()
    {
        return $this->pedidos;
    }

    public function __construct()
    {
        $this->enderecos = new ArrayCollection();
        $this->pedidos = new ArrayCollection();
    }
}
