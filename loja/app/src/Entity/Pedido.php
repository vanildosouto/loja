<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="pedidos")
 * @ORM\HasLifecycleCallbacks
 */
class Pedido
{
    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var datetime
     */
    private $dataHora;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="pedidos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Endereco", inversedBy="pedidos")
     * @ORM\JoinColumn(name="endereco_id", referencedColumnName="id")
     */
    private $endereco;

    /**
     * @ORM\ManyToMany(targetEntity="Produto", inversedBy="pedidos")
     * @ORM\JoinTable(name="pedido_produtos")
     */
    private $produtos;

    public function setId($id)
    {
        return $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDataHora($dataHora)
    {
        return $this->dataHora = $dataHora;
    }

    public function getDataHora()
    {
        return $this->dataHora;
    }

    public function setUsuario($usuario)
    {
        return $this->usuario = $usuario;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setEndereco($endereco)
    {
        return $this->endereco = $endereco;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function setProdutos($produtos)
    {
        return $this->produtos = $produtos;
    }

    public function getProdutos()
    {
        return $this->produtos;
    }

    public function __construct()
    {
        $this->produtos = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($this->getDataHora() == null) {
            $this->setDataHora(new \DateTime("now"));
        }
    }
}
