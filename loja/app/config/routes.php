<?php

$app->get("/", App\Action\Home::class);
$app->get('/categoria/{categoria}', App\Action\Categoria::class);
$app->get('/produto/{id:[0-9]+}', App\Action\Produto::class);
$app->post('/carrinho/adicionar', App\Action\Carrinho\Adicionar::class);
$app->get('/carrinho', App\Action\Carrinho\Listar::class);

$app->post('/busca', App\Action\Busca::class);
