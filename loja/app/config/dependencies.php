<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

$container = $app->getContainer();

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger   = new \Monolog\Logger($settings['name']);
    $filename = $settings['filename'];

    $stream = new \Monolog\Handler\StreamHandler(
        $settings['filename'],
        $settings['level'],
        true,
        0666
    );

    $logger->pushHandler($stream);
    return $logger;
};

$container['view'] = function ($c) {
    $view_config = $c->get('settings')['view'];

    $view = new \Slim\Views\Twig($view_config['path'], [
        'cache' => $view_config['cache'],
        'debug' => $view_config['debug']
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $c['router'],
        $c['request']->getUri()
    ));

    $categorias = $c['em']->getRepository('App\Entity\Categoria')->findAll();

    $messages = $c['flash']->getMessages();
    $env = $view->getEnvironment();
    $env->addGlobal('messages', $messages);
    $env->addGlobal('categorias', $categorias);
    $env->addGlobal('session', $_SESSION);

    return $view;
};

$container['em'] = function ($c) {
    $doctrine_config = $c->get('settings')['doctrine'];

    $config = Setup::createConfiguration($doctrine_config['devmode']);
    $driver = new AnnotationDriver(new AnnotationReader(), $doctrine_config['paths']);

    $config->setMetadataDriverImpl($driver);
    $config->setAutoGenerateProxyClasses(true);
    AnnotationRegistry::registerFile(
        __DIR__ . '/../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
    );

    return EntityManager::create($doctrine_config['dbconf'], $config);
};

$container['acl'] = function ($c) {
    $acl = new Toneladas\Acl();
    $acl->setWithDoctrine($c['em']);
    $acl->setEntity('App\Entity\Users');
    $acl->setFieldUser('email');
    $acl->setMethodPassword('getSenha');

    return $acl;
};

$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages;
};

################
### Actions ####
################

$container[App\Action\Home::class] = function ($c) {
    return new App\Action\Home($c['logger'], $c['view']);
};

$container[App\Action\Categoria::class] = function ($c) {
    return new App\Action\Categoria($c['logger'], $c['view'], $c['em']);
};

$container[App\Action\Busca::class] = function ($c) {
    return new App\Action\Busca($c['logger'], $c['view'], $c['em']);
};

$container[App\Action\Produto::class] = function ($c) {
    return new App\Action\Produto($c['logger'], $c['view'], $c['em']);
};

$container[App\Action\Carrinho\Adicionar::class] = function ($c) {
    return new App\Action\Carrinho\Adicionar($c['logger'], $c['view']);
};

$container[App\Action\Carrinho\Listar::class] = function ($c) {
    return new App\Action\Carrinho\Listar($c['logger'], $c['view'], $c['em']);
};
