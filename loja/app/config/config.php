<?php

return [
    'settings' => [
        'displayErrorDetails' => getenv("SLIM_DEBUG"),
        'view' => [
            'cache' => false,
            'debug' => true,
            'path'  => __DIR__ . '/../templates',
        ],
        'doctrine' => [
            'devmode' => getenv("DOCTRINE_DEVMODE"),
            'paths'   => [__DIR__ . '/../src/Entity'],
            'dbconf'  => [
                'driver' => getenv("DOCTRINE_DRIVER"),
                'url'    => getenv("DOCTRINE_URL")
            ]
        ],
        'logger'   => [
            'name'     => getenv("LOGGER_NAME"),
            'filename' => __DIR__  . '/../../logs/app-' . getenv("LOGGER_NAME") . '.log',
            'level'    => \Monolog\Logger::DEBUG
        ],
    ]
];
