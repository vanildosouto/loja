<?php

namespace Test;

use App\Entity\Categoria;

class CategoriaTest extends TestCase
{
    public function testAddCategoria()
    {
        $celular = new Categoria;
        $celular->setNome("Celular");
        $celular->setSlug("celulares");

        $this->getEntityManager()->persist($celular);
        $this->getEntityManager()->flush();

        $this->assertNotNull($celular->getId());
        $this->assertEquals(1, $celular->getId());

        $savedCelular = $this->getEntityManager()->find(get_class($celular), $celular->getId());

        $this->assertInstanceOf(get_class($celular), $savedCelular);
        $this->assertEquals($celular->getNome(), $savedCelular->getNome());
    }
}
