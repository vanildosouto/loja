<?php

namespace Test;

use App\Entity\Caracteristica;

class CaracteristicaTest extends TestCase
{
    public function testAddCaracteristica()
    {
        $peso = new Caracteristica;
        $peso->setNome("Peso");

        $this->getEntityManager()->persist($peso);
        $this->getEntityManager()->flush();

        $this->assertNotNull($peso->getId());
        $this->assertEquals(1, $peso->getId());

        $savedPeso = $this->getEntityManager()->find(get_class($peso), $peso->getId());

        $this->assertInstanceOf(get_class($peso), $savedPeso);
        $this->assertEquals($peso->getNome(), $savedPeso->getNome());
    }
}
