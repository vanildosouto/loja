<?php

namespace Test;

use App\Entity\Produto;
use App\Entity\Categoria;

class ProdutoTest extends TestCase
{
    public function testAddProduto()
    {
        $celular = new Categoria;
        $celular->setNome("Celular");
        $celular->setSlug("celulares");

        $this->getEntityManager()->persist($celular);

        $eletronico = new Categoria;
        $eletronico->setNome("Eletronico");
        $eletronico->setSlug("eletronicos");

        $this->getEntityManager()->persist($eletronico);

        $motorola = new Produto;
        $motorola->setNome('Motorola G5');
        $motorola->setDescricao('Novo aparelho da linha G da Motorola');
        $motorola->setImagem('motog5.png');
        $motorola->setPreco(899.99);
        $motorola->setCategorias([$celular, $eletronico]);

        $this->getEntityManager()->persist($motorola);
        $this->getEntityManager()->flush();

        $this->assertNotNull($motorola->getId());
        $this->assertEquals(1, $motorola->getId());

        $savedMotorola = $this->getEntityManager()->find(get_class($motorola), $motorola->getId());

        $this->assertInstanceOf(get_class($motorola), $savedMotorola);
        $this->assertEquals($motorola->getNome(), $savedMotorola->getNome());
        $this->assertEquals($motorola->getDescricao(), $savedMotorola->getDescricao());
        $this->assertEquals($motorola->getImagem(), $savedMotorola->getImagem());
        $this->assertEquals($motorola->getPreco(), $savedMotorola->getPreco());
        $this->assertEquals($motorola->getCategorias(), $savedMotorola->getCategorias());
    }
}
