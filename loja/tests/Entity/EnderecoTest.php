<?php

namespace Test;

use App\Entity\Endereco;

class EnderecoTest extends TestCase
{
    public function testAddEndereco()
    {
        $endereco = new Endereco;
        $endereco->setEndereco("Avenida Paulista");
        $endereco->setNumero("1200");
        $endereco->setComplemento("12º andar");
        $endereco->setCep("01111-111");
        $endereco->setBairro("Bela Vista");
        $endereco->setCidade("São Paulo");
        $endereco->setEstado("SP");

        $this->getEntityManager()->persist($endereco);
        $this->getEntityManager()->flush();

        $this->assertNotNull($endereco->getId());
        $this->assertEquals(1, $endereco->getId());

        $savedEndereco = $this->getEntityManager()->find(get_class($endereco), $endereco->getId());

        $this->assertInstanceOf(get_class($endereco), $savedEndereco);
        $this->assertEquals($endereco->getEndereco(), $savedEndereco->getEndereco());
        $this->assertEquals($endereco->getNumero(), $savedEndereco->getNumero());
        $this->assertEquals($endereco->getComplemento(), $savedEndereco->getComplemento());
        $this->assertEquals("01111111", $savedEndereco->getCep());
        $this->assertEquals($endereco->getBairro(), $savedEndereco->getBairro());
        $this->assertEquals($endereco->getCidade(), $savedEndereco->getCidade());
        $this->assertEquals($endereco->getEstado(), $savedEndereco->getEstado());
    }
}
