<?php

namespace Test;

use App\Entity\Usuario;
use App\Entity\Endereco;
use App\Entity\Produto;
use App\Entity\Categoria;
use App\Entity\Pedido;

class PedidoTest extends TestCase
{
    public function testAddPedido()
    {
        $usuario = $this->buildUsuario();
        $produto = $this->buildProduto();

        $pedido = new Pedido;
        $pedido->setUsuario($usuario);
        $pedido->setEndereco($usuario->getEnderecos()[0]);
        $pedido->setProdutos([$produto]);

        $this->getEntityManager()->persist($pedido);
        $this->getEntityManager()->flush();

        $this->assertNotNull($pedido->getId());
        $this->assertEquals(1, $pedido->getId());

        $savedPedido = $this->getEntityManager()->find(get_class($pedido), $pedido->getId());

        $this->assertInstanceOf(get_class($pedido), $savedPedido);
        $this->assertEquals($pedido->getUsuario(), $savedPedido->getUsuario());
        $this->assertEquals($pedido->getEndereco(), $savedPedido->getEndereco());
        $this->assertEquals($pedido->getProdutos(), $savedPedido->getProdutos());
    }

    public function buildEndereco()
    {
        $endereco = new Endereco;
        $endereco->setEndereco("Avenida Paulista");
        $endereco->setNumero("1200");
        $endereco->setComplemento("12º andar");
        $endereco->setCep("01111-111");
        $endereco->setBairro("Bela Vista");
        $endereco->setCidade("São Paulo");
        $endereco->setEstado("SP");

        $this->getEntityManager()->persist($endereco);

        return $endereco;
    }

    public function buildUsuario()
    {
        $usuario = new Usuario;
        $usuario->setNome("João da Silva");
        $usuario->setCpf("503.105.061-66");
        $usuario->setEmail("joao@gmail.com");
        $usuario->setSenha("senhasegura");

        $endereco = $this->buildEndereco();

        $usuario->setEnderecos([$endereco]);

        $this->getEntityManager()->persist($usuario);

        return $usuario;
    }

    public function buildProduto()
    {
        $celular = new Categoria;
        $celular->setNome("Celular");
        $celular->setSlug("celulares");

        $this->getEntityManager()->persist($celular);

        $motorola = new Produto;
        $motorola->setNome('Motorola G5');
        $motorola->setDescricao('Novo aparelho da linha G da Motorola');
        $motorola->setImagem('motog5.png');
        $motorola->setPreco(899.99);
        $motorola->setCategorias([$celular]);

        $this->getEntityManager()->persist($motorola);

        return $motorola;
    }
}
