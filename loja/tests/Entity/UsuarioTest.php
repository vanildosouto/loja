<?php

namespace Test;

use App\Entity\Usuario;
use App\Entity\Endereco;

class UsuarioTest extends TestCase
{
    public function testAddUsuario()
    {
        $usuario = new Usuario;
        $usuario->setNome("João da Silva");
        $usuario->setCpf("503.105.061-66");
        $usuario->setEmail("joao@gmail.com");
        $usuario->setSenha("senhasegura");

        $endereco = $this->buildEndereco();

        $usuario->setEnderecos([$endereco]);

        $this->getEntityManager()->persist($usuario);
        $this->getEntityManager()->flush();

        $this->assertNotNull($usuario->getId());
        $this->assertEquals(1, $usuario->getId());

        $savedUsuario = $this->getEntityManager()->find(get_class($usuario), $usuario->getId());

        $this->assertInstanceOf(get_class($usuario), $savedUsuario);
        $this->assertEquals($usuario->getNome(), $savedUsuario->getNome());
        $this->assertEquals('50310506166', $savedUsuario->getCpf());
        $this->assertEquals($usuario->getEmail(), $savedUsuario->getEmail());
        $this->assertEquals($usuario->getEnderecos(), $savedUsuario->getEnderecos());
        $this->assertTrue(password_verify('senhasegura', $savedUsuario->getSenha()));
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Endereço de email inválido
     */
    public function testAddUsuarioEmailInvalido()
    {
        $usuario = new Usuario;
        $usuario->setEmail("joaogmail.com");
    }

    public function buildEndereco()
    {
        $endereco = new Endereco;
        $endereco->setEndereco("Avenida Paulista");
        $endereco->setNumero("1200");
        $endereco->setComplemento("12º andar");
        $endereco->setCep("01111-111");
        $endereco->setBairro("Bela Vista");
        $endereco->setCidade("São Paulo");
        $endereco->setEstado("SP");

        $this->getEntityManager()->persist($endereco);

        return $endereco;
    }
}
