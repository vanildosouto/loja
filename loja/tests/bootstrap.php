<?php

require __DIR__ . '/../vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

//se for falso usa o APC como cache, se for true usa cache em arrays
$isDevMode = false;

// Caminho das entidades
$paths = [__DIR__ . '/../app/src/Entity'];

$config = Setup::createConfiguration($isDevMode);

// Leitor das annotations das entidades
$driver = new AnnotationDriver(new AnnotationReader(), $paths);
$config->setMetadataDriverImpl($driver);

// Registra as annotations do Doctrine
AnnotationRegistry::registerFile(
    __DIR__ . '/../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
);

// Configuracoes do banco de dados
$dbParams = [
    'driver' => 'pdo_sqlite',
    'dbname' => 'memory:dnp.db',
];

return $entityManager = EntityManager::create($dbParams, $config);
