<?php

namespace Fixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\Produto;

class Produtos extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $produtos = [
            [
                'nome'       => 'Moto G5',
                'descricao'  => 'Novo aparelho da linha G da Motorola',
                'imagem'     => 'moto5.png',
                'preco'      => 899.99,
                'categorias' => [$this->getReference('celulares'), $this->getReference('lancamentos')]
            ],
            [
                'nome'       => 'Samsung Galaxy S8',
                'descricao'  => 'O design revolucionário do Galaxy S8 e S8+ começa de dentro para fora. Remodelamos cada peça do layout do aparelho para ultrapassar os limites da tela do aparelho. Assim, tudo o que você vê é puro conteúdo, e praticamente nenhuma moldura.',
                'imagem'     => 'samsungs8.png',
                'preco'      => 2799.99,
                'categorias' => [$this->getReference('celulares'), $this->getReference('lancamentos')]
            ],
            [
                'nome'       => 'Moto C Plus',
                'descricao'  => 'Diga hello ao Moto C Plus! Um novo smartphone que traz tudo o que você sempre quis, sem custar muito por isso.',
                'imagem'     => 'motoc.png',
                'preco'      => 699.99,
                'categorias' => [$this->getReference('celulares')]
            ],
            [
                'nome'       => '49” MU6120 Smart 4K UHD TV',
                'descricao'  => 'TV Samsung HDR Premium com 4K de Verdade, cores naturais e última tendência em brilho e contraste.',
                'imagem'     => 'samsung49.png',
                'preco'      => 4699.99,
                'categorias' => [$this->getReference('televisores'), $this->getReference('lancamentos')]
            ],
            [
                'nome'       => '58” MU6120 Smart 4K UHD TV',
                'descricao'  => 'A TV Samsung MU6120 garante o melhor da tecnologia UHD, com paineis RGB e com fidelidade de cores, sem sub-píxel branco. Certificada pelas principais associações internacionais do setor.',
                'imagem'     => 'samsung58.png',
                'preco'      => 5999.99,
                'categorias' => [$this->getReference('televisores')]
            ],
            [
                'nome'       => 'Xbox One X',
                'descricao'  => 'O console mais poderoso do mundo. Experimente jogos imersivos reais em 4K, 40% mais potente que qualquer outro console. Os jogos ficam melhores no Xbox One X.',
                'imagem'     => 'xboxonex.png',
                'preco'      => 2999.99,
                'categorias' => [$this->getReference('videogames'), $this->getReference('lancamentos')]
            ],
            [
                'nome'       => 'Playstation 4 Pro',
                'descricao'  => 'A PS4 superpotente: mais rápida, mais poderosa e com jogos 4K*.',
                'imagem'     => 'playstation4pro.png',
                'preco'      => 2999.99,
                'categorias' => [$this->getReference('videogames'), $this->getReference('lancamentos')]
            ],
            [
                'nome'       => 'Playstation 4',
                'descricao'  => 'PS4 - a consola mais vendida do mundo – agora mais fina e mais leve, com um fantástico poder de jogo nas versões de 500 GB e de 1 TB. Já disponíveis em Jet Black, Glacier White e nos deslumbrantes tons Gold e Silver.',
                'imagem'     => 'playstation4.png',
                'preco'      => 1999.99,
                'categorias' => [$this->getReference('videogames')]
            ],
        ];

        foreach ($produtos as $id => $produto) {
            $novoProduto = new Produto;
            $novoProduto->setNome($produto['nome']);
            $novoProduto->setDescricao($produto['descricao']);
            $novoProduto->setImagem($produto['imagem']);
            $novoProduto->setPreco($produto['preco']);
            $novoProduto->setCategorias($produto['categorias']);
            $manager->persist($novoProduto);

            $this->addReference('produto-' . $id, $novoProduto);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
