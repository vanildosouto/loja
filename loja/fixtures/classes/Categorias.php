<?php

namespace Fixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\Categoria;

class Categorias extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categorias = [
            [
                'nome' => 'Celular',
                'slug' => 'celulares',
            ],
            [
                'nome' => 'Video game',
                'slug' => 'videogames',
            ],
            [
                'nome' => 'Televisor',
                'slug' => 'televisores',
            ],
            [
                'nome' => 'Lançamentos',
                'slug' => 'lancamentos'
            ]
        ];

        foreach ($categorias as $categoria) {
            $categ = new Categoria;
            $categ->setNome($categoria['nome']);
            $categ->setSlug($categoria['slug']);
            $manager->persist($categ);

            $this->addReference($categoria['slug'], $categ);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
