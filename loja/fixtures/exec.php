<?php

include __DIR__ . '/../vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;

$config = include __DIR__ . '/../app/config/config.php';

$doctrine_config = $config['settings']['doctrine'];

$config = Setup::createConfiguration($doctrine_config['devmode']);
$driver = new AnnotationDriver(new AnnotationReader(), $doctrine_config['paths']);

$config->setMetadataDriverImpl($driver);
$config->setAutoGenerateProxyClasses(true);
AnnotationRegistry::registerFile(
    __DIR__ . '/../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
);

$em = EntityManager::create($doctrine_config['dbconf'], $config);

// Crio o banco
$tool = new \Doctrine\ORM\Tools\SchemaTool($em);
$classes = $em->getMetadataFactory()->getAllMetadata();
$tool->dropSchema($classes);
$tool->createSchema($classes);

$loader = new Loader();
$loader->loadFromDirectory(__DIR__ . '/classes');

$purger = new ORMPurger();
$executor = new ORMExecutor($em, $purger);
$executor->execute($loader->getFixtures());
