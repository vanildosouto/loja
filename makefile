default:
	@cli/dev/docker-iniciar-container.sh
	@echo Site disponível em: http://183.15.0.20/
create:
	@cli/dev/docker-criar-container.sh
	@cd loja && php composer.phar install
	@docker exec -it loja-app bash -c "php /var/www/app/fixtures/exec.php"
destroy:
	@cli/dev/docker-destruir-container.sh
build:
	@docker build -t loja docker/
tests:
	@cd loja/tests/ && ../vendor/bin/phpunit .
