#!/bin/bash

docker exec -it loja-app bash -c "cd /var/www/app && vendor/bin/doctrine $1 $2"
