#!/bin/bash

docker network create --subnet=183.15.0.0/16 toneladas-apps

# Container da aplicação
docker run --network toneladas-apps --ip 183.15.0.20 --name loja-app --env-file $(pwd)/$(dirname $0)/env -itd -v $(pwd)/$(dirname $0)/../../loja:/var/www/app loja

# Crio o container do Mysql
docker run --network toneladas-apps --ip 183.15.0.19 --name loja-db -v $(pwd)/$(dirname $0)/../../.banco:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=gNbqZL73CSHGKeEUZNcHg -e MYSQL_DATABASE=loja -d mariadb:latest

mkdir -p $(dirname $0)/../../loja/logs
>>$(dirname $0)/../../loja/logs/app-dev.log
chmod -R 777 $(dirname $0)/../../loja/logs/app-dev.log
