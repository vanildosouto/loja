#!/bin/bash

docker start loja-app
docker start loja-db

mkdir -p $(dirname $0)/../../loja/logs
>>$(dirname $0)/../../loja/logs/app-dev.log
chmod -R 777 $(dirname $0)/../../loja/logs/app-dev.log
