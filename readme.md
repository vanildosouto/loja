# Projeto Loja

Projeto de uma loja simples

## Iniciando o projeto

Para rodar o projeto, rode os comandos na pasta raiz do projeto:

`make build`

`make create`

O site estará disponível em *http://183.15.0.20/*

## Rodando os testes

Após a instalação, para rodar os testes execute:

`make tests`
